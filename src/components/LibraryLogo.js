import Grid from '@mui/material/Grid';

export default function LibraryLogo(props) {
  return (
    <Grid item xl={6}>
      <img alt="Duke University Libraries" src="https://library.duke.edu/masthead/img/logo.png" />
    </Grid>
  );
}
