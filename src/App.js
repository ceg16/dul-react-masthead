import React, { Component } from 'react';
import masthead from './mastheadData.json';
import AppBar from '@mui/material/AppBar';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import LibraryLogo from './components/LibraryLogo';
import ScopedSearch from './components/ScopedSearch';
import MyAccount from './components/MyAccount';
import MegaMenu from './components/MegaMenu';

class App extends Component {

  state = masthead;

  render() {
    return (
      <AppBar color="primary"
        sx={{
          paddingTop: "1.5rem",
          paddingBottom: "1rem",
        }}
      >
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Grid container direction="row" maxWidth="xl">
              <LibraryLogo></LibraryLogo>
              <Grid item xl={6}>
                <Grid container direction="row" mb={2}>
                  <MyAccount></MyAccount>
                  <ScopedSearch></ScopedSearch>
                  <MegaMenu></MegaMenu>
                </Grid>
              </Grid>
            </Grid>
          </Toolbar>
        </Container>
      </AppBar>
    );
  }
}

export default App;
